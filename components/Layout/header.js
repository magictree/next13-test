import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Link from "next/link";
import React, {useEffect, useState} from "react";
import {Button, Col, Dropdown, Row} from "react-bootstrap";

function Header() {
  const [name, setname] = useState("A");
  const [email, setemail] = useState("-");

  useEffect(() => {
    setname(localStorage.getItem("fullname") ?? "A");
    setemail(localStorage.getItem("email") ?? "a@a.com");
  }, []);

  return (
    <div className="headcontainer" style={{marginLeft: 0}}>
      <div className="headwrapper">
        <div className="title text-left ml-5"></div>
        <div className="profile hidden-desktop hidden-print">
          <Row>
            <Col>
              <div className="title text-right">
                <h6>{name}</h6>
                <p>{email}</p>
              </div>
            </Col>
            <Col>
              <Dropdown>
                <Dropdown.Toggle as={Button} className="profile-icon">
                  {name.charAt(0) ?? "-"}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item id="sub-3" as={Link} href={`/logout`}>
                    <FontAwesomeIcon icon={faSignOutAlt} width={18} />
                    &nbsp; Logout
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}

export default Header;
