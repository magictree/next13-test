import Header from "../Layout/header";

export default function Layout(props) {
  const {children} = props;
  return (
    <div>
      <Header />
      <div>{children}</div>
    </div>
  );
}
