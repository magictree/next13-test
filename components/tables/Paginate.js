import React, {useEffect, useState} from "react";
import {Pagination, Table} from "react-bootstrap";

const Paginate = ({pageChangeHandler, selectedPages, totalRows, rowsPerPage, pages}) => {
  // Calculating max number of pages
  //const pages = Math.ceil(totalRows / rowsPerPage);

  // Creating an array with length equal to no.of pages
  //const pagesArr = [...new Array(pages)];

  // State variable to hold the current page. This value is
  // passed to the callback provided by the parent
  const [currentPage, setCurrentPage] = useState(selectedPages);
  const [noOfPages, setNoOfPages] = useState(pages);
  const [pageSize, setPageSize] = useState(rowsPerPage);
  // console.info(noOfPages,pages,'total Pages', totalRows, rowsPerPage);
  // console.info("page size", pageSize)
  // console.info("row per page", rowsPerPage)
  // Navigation arrows enable/disable state
  const [canGoBack, setCanGoBack] = useState(false);
  const [canGoNext, setCanGoNext] = useState(true);

  // Onclick handlers for the butons
  const onNextPage = () => setCurrentPage(currentPage + 1);
  const onPrevPage = () => setCurrentPage(currentPage - 1);
  const onPageSelect = (pageNo) => setCurrentPage(pageNo);
  const onGoPage = (value) => setCurrentPage(value + 1);

  // Disable previous and next buttons in the first and last page
  // respectively
  useEffect(() => {
    if (noOfPages === currentPage) {
      setCanGoNext(false);
    } else {
      setCanGoNext(true);
    }
    if (currentPage === 1) {
      setCanGoBack(false);
    } else {
      setCanGoBack(true);
    }
  }, [noOfPages, currentPage]);

  // To set the starting index of the page
  useEffect(() => {
    const skipFactor = (currentPage - 1) * rowsPerPage;
    // Some APIs require skip for paginaiton. If needed use that instead
    // pageChangeHandler(skipFactor);
    pageChangeHandler({page: currentPage, limit: pageSize});
  }, [currentPage, pageSize]);

  return (
    <>
      {noOfPages > 1 ? (
        <div className="paginations">
          <Table className="borderless nowrap">
            <tbody>
              <tr>
                <td
                  style={{
                    width: "100%",
                  }}
                >
                  <Pagination className="float-left">
                    {/* <Pagination.Item onClick={() => onGoPage(0)} disabled={!canGoBack}>
                      {" "}
                      {`<<`}{" "}
                    </Pagination.Item> */}
                    <Pagination.Item onClick={() => onPrevPage()} disabled={!canGoBack}>
                      {" "}
                      {`<`}{" "}
                    </Pagination.Item>
                    <Pagination.Item onClick={() => onNextPage()} disabled={!canGoNext}>
                      {" "}
                      {`>`}{" "}
                    </Pagination.Item>
                    {/* <Pagination.Item onClick={() => onGoPage(pages - 1)} disabled={!canGoNext}>
                      {" "}
                      {`>>`}{" "}
                    </Pagination.Item> */}
                  </Pagination>
                </td>
                <td>
                  {/* <span
                    className="mx-2"
                    style={{
                      height: "32.375px",
                      display: "inline-flex",
                      alignItems: "center",
                    }}
                  >
                    Page &nbsp;
                    <strong>
                      {currentPage} of {pages}
                    </strong>
                  </span> */}
                </td>
                <td>
                  {/* <span className="mx-2">
                    Go to page :{" "}
                    <input
                      type="number"
                      defaultValue={currentPage}
                      onChange={(e) => {
                        const page = e.target.value ? Number(e.target.value) - 1 : 0;
                        onGoPage(page);
                      }}
                      style={{
                        width: "80px",
                        height: "32.375px",
                        paddingLeft: "3px",
                      }}
                    />
                  </span> */}
                </td>
                <td>
                  {/* <select
                    value={pageSize}
                    onChange={(e) => {
                      //console.log(e.target , 9090)
                      setPageSize(Number(e.target.value));
                      setNoOfPages(Math.ceil(totalRows / Number(e.target.value)));
                    }}
                    style={{height: "32.375px"}}
                  >
                    {[10, 20, 30, 40, 50].map((pageSize) => (
                      <option key={pageSize} value={pageSize}>
                        Show {pageSize}
                      </option>
                    ))}
                  </select> */}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      ) : null}
    </>
  );
};

export default Paginate;
