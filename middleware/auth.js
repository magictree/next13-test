import nextConnect from "next-connect";
import {authVerify_Service} from "../pages/api/auth/auth.service";

const middleware = nextConnect();

export default middleware.use(async (req, res, next) => {
  let authHeader = req.headers.authorization || "";
  let result = {};

  if (authHeader) {
    let sessionID = authHeader.split(" ")[1];
    if (sessionID) {
      result = await authVerify_Service({token: sessionID});
      if (!result.error) {
        req.user = result.data;
      } else {
        res.statusCode = 401;
        return res.send(result);
      }
    } else {
      res.statusCode = 401;
      return res.send({
        status: "error",
        error: "Invalid",
      });
    }
  } else {
    res.statusCode = 401;
    return res.send({
      status: "error",
      error: "Unauthorized",
    });
  }
  return next();
});
