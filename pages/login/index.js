/* eslint-disable @next/next/no-html-link-for-pages */
import {useState, Fragment} from "react";
import {Button, Card, Col, Form, Row, Spinner} from "react-bootstrap";
import bg from "../../public/images/bg.png";
import {useRouter} from "next/router";

import {toast} from "react-toastify";
import Head from "next/head";
import {setCookie} from "cookies-next";

export default function Login() {
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();

  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");

  const actionLogin = async (elemet) => {
    setIsLoading(true);
    elemet.preventDefault();

    const options = {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    };
    const req = await fetch(`/api/auth`, options);
    const res = await req.json();
    if (!res.error) {
      // set to client side
      setCookie("token", res.data.token);
      setCookie("fullname", res.data.fullname);
      setCookie("email", res.data.email);
      localStorage.setItem("fullname", res.data.fullname);
      localStorage.setItem("email", res.data.email);
      router.push("/");
      setIsLoading(false);
    } else {
      toast(res.message);
      setIsLoading(false);
    }
  };

  const enterActionLogin = async (element) => {
    if (element.keyCode === 13) {
      actionLogin(element);
    }
  };

  return (
    <Fragment>
      <Head>
        <title>Login</title>
        <link rel="shortcut icon" href="/images/LOGO.png" />
      </Head>
      <div
        style={{
          backgroundImage: `url(${bg.src})`,
          width: "100%",
          height: "100%",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
        }}
      >
        <div className="auth-wrapper">
          <div className=" mb-4" style={{display: "block", width: "80%"}}>
            <Row>
              <Col
                xs={12}
                sm={12}
                md={12}
                lg={6}
                xl={6}
                className="hidden-desktop hidden-tablet"
              ></Col>

              <Col xs={12} sm={12} md={12} lg={6} xl={6}>
                <div className="login-responsive">
                  <Card style={{width: "100%"}}>
                    <Card.Body>
                      <Card.Title className="text-center font-weight-bold p-5">Login</Card.Title>

                      {/* <Form> */}
                      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <small>Email</small>
                        <Form.Control
                          value={email}
                          onChange={(e) => setemail(e.target.value)}
                          type="email"
                          placeholder="name@example.com"
                        />
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="formBasicPassword">
                        <small>Password</small>
                        <Form.Control
                          value={password}
                          onChange={(e) => setpassword(e.target.value)}
                          onKeyDown={(e) => enterActionLogin(e)}
                          type="password"
                          placeholder="Password"
                        />
                      </Form.Group>
                      <div className="d-flex justify-content-center py-3">
                        <Button
                          onClick={(e) => actionLogin(e)}
                          size="lg"
                          className="px-5 py-3 btn-prima"
                          style={{borderRadius: "15px"}}
                        >
                          {isLoading ? <Spinner animation="border" /> : "LOGIN"}
                        </Button>
                      </div>
                      {/* </Form> */}
                    </Card.Body>
                  </Card>
                </div>
              </Col>
            </Row>
          </div>
        </div>
        <footer style={{padding: "15px", backgroundColor: "white"}}>
          <div className="footer-copyright text-center">Copyright © 2023</div>
        </footer>
      </div>
    </Fragment>
  );
}
