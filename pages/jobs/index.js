import Head from "next/head";
import {Fragment, useEffect, useState} from "react";
import {Button, Card, Col, Figure, Modal, Row} from "react-bootstrap";
import Header from "../../components/Layout/header";
import styles from "../../styles/Home.module.css";

import {toast} from "react-toastify";
import Paginate from "../../components/tables/Paginate";

import {faDiagnoses, faList} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {getCookie} from "cookies-next";
import Link from "next/link";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

export async function getServerSideProps(context) {
  const {req, res} = context;
  const token = getCookie("token", {req, res}) || null;
  let dataPaginate = {
    total: 1,
    pages: 1,
    limit: 10,
  };

  const options = {
    headers: {
      Authorization: `Bearer ${token || ""}`,
    },
  };
  const fetchJobs = await fetch(
    `${process.env.BASE_URL}/api/jobs?page=${dataPaginate.pages}`,
    options
  );
  let resultJobs = await fetchJobs.json();

  if (token) {
    return {
      props: {
        jobs: resultJobs.data.result,
        pagination: resultJobs.data.paginate,
        token: token,
      },
    };
  } else {
    return {
      redirect: {
        permanent: true,
        destination: "/login",
      },
      props: {},
    };
  }
}

export default function Jobs(props) {
  const mediaDoc = "http://localhost:3000/SQLTest.pdf";
  const [media, setMedia] = useState(false);
  const [Loading, setLoading] = useState(false);
  const {jobs, token, pagination} = props;
  const [rows, setrows] = useState(jobs);
  const [detail, setdetail] = useState(false);
  const [filterDescription, setFilterDescription] = useState("");
  const [filterLocation, setFilterLocation] = useState("");
  const [filterType, setFilterType] = useState(false);
  const [pageLimit, setPageLimit] = useState(pagination.limit);

  const [pageCurrent, setPageCurrent] = useState(pagination.page);
  const [dataPaginate, setDataPaginate] = useState(pagination);

  const [jobId, setJobId] = useState(false);

  useEffect(() => {
    return () => {
      true;
    };
  }, []);

  const modalDetail = async (element, value, isOpen) => {
    element.preventDefault();
    setIsScoring(isOpen);
    let options = {
      headers: {
        Authorization: `Bearer ${token || ""}`,
      },
    };
    const req = await fetch(`/api/jobs/${value}`, options);
    const res = await req.json();
    setdetail(res);
  };

  const query_url = async ({page, filterDescription, filterLocation, filterType}) => {
    console.info(pageCurrent, dataPaginate.page);

    let url = "?&";
    url += `&filterFullTimeOnly=${filterType}&`;

    if (filterDescription !== "") url += `&filterDescription=${filterDescription}&`;
    if (filterLocation !== "") url += `&filterLocation=${filterLocation}&`;
    if (page) url += `&page=${page}&`;

    if (url === "?") url = "";

    return url;
  };

  const onSearch = async (e) => {
    e.preventDefault();
    await pageChange({...dataPaginate.page});
  };

  const getJob = async (query) => {
    let options = {
      headers: {
        Authorization: `Bearer ${token || ""}`,
      },
    };

    const reqJobs = await fetch(`${process.env.BASE_URL}/api/jobs${query}`, options);
    let result = await reqJobs.json();
    if (result.data.result.error) {
      toast.warning("data is empty");
      setrows([]);
    } else {
      setrows(result.data.result);
      setDataPaginate(result.data.paginate);
    }
  };

  const getDetail = async (id) => {
    setJobId(id);
    let options = {
      headers: {
        Authorization: `Bearer ${token || ""}`,
      },
    };

    const reqJobs = await fetch(`${process.env.BASE_URL}/api/jobs/${id}`, options);
    let result = await reqJobs.json();
    if (result.error) {
      toast.warning("data is empty");
      await pageChange();
    } else {
      setdetail(result.data);
    }
  };

  const closeDetail = async (id) => {
    setJobId(false);
    setdetail(false);
    await pageChange({page: dataPaginate.page});
  };

  const closeMedia = async (id) => {
    setMedia(false);
  };

  const openMedia = async (id) => {
    setMedia(mediaDoc);
  };

  const pageChange = async ({page}) => {
    if (page) {
      setPageCurrent(page);
      setDataPaginate({...dataPaginate, page});
    } else {
      page = page || dataPaginate.page;
    }

    const query = await query_url({page, filterDescription, filterLocation, filterType});
    await getJob(query);
  };

  return (
    <Fragment>
      <Head>
        <title>TEST</title>
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <Header style={{marginLeft: "0"}} />
      <Fragment>
        <div className={styles.contentcontainer} style={{marginLeft: "0"}}>
          <div className={styles.contentwrapper} style={{paddingTop: "20px"}}>
            <div className="p-2 mb-2">
              <h4>Main</h4>
            </div>
            <Card>
              <Card.Header
                style={{
                  backgroundColor: "white",
                  borderRadius: "9px 9px 0 0",
                }}
              >
                <Button as={Link} href={`/jobs`} style={{padding: "9px 19px", borderRadius: "6px"}}>
                  <FontAwesomeIcon width={18} icon={faDiagnoses} /> Job List
                </Button>
                <Button
                  variant="light"
                  style={{padding: "9px 19px", borderRadius: "6px"}}
                  className="ml-3"
                  onClick={openMedia}
                >
                  <FontAwesomeIcon width={18} icon={faDiagnoses} /> SQL Test
                </Button>
              </Card.Header>
              {Loading ? (
                <Card.Body>
                  <Skeleton className="mb-3" height={40} style={{borderRadius: 10}} />
                  <Row className="mb-3">
                    <Col lassName="hidden-print" xs={12} sm={12} md={3}>
                      <Skeleton height={20} count={20} style={{borderRadius: 10, marginTop: 10}} />
                    </Col>
                    <Col xs={12} sm={12} md={3}>
                      <Skeleton
                        height={600}
                        style={{borderRadius: 10}}
                        // width={200}
                      />
                    </Col>
                    <Col xs={12} sm={12} md={3}>
                      <Skeleton
                        height={600}
                        style={{borderRadius: 10}}
                        // width={200}
                      />
                    </Col>
                    <Col xs={12} sm={12} md={3}>
                      <Skeleton
                        height={600}
                        style={{borderRadius: 10}}
                        // width={200}
                      />
                    </Col>
                  </Row>
                </Card.Body>
              ) : (
                <Card.Body>
                  <Row className="pb-5">
                    <Col xs={12} sm={6} md={4} lg={3} xl={2} key="Job description">
                      <div className="form-group">
                        <label>Job description</label>
                        <input
                          className="form-control"
                          type="text"
                          name="filterDescription"
                          value={filterDescription}
                          placeholder="Filter by title, companies"
                          onChange={(e) => setFilterDescription(e.target.value)}
                        />
                      </div>
                    </Col>
                    <Col xs={12} sm={6} md={4} lg={3} xl={2} key="Job location">
                      <div className="form-group">
                        <label>Location</label>
                        <input
                          className="form-control"
                          type="text"
                          name="filterLocation"
                          value={filterLocation}
                          placeholder="Filter by location"
                          onChange={(e) => setFilterLocation(e.target.value)}
                        />
                      </div>
                    </Col>
                    <Col xs={12} sm={6} md={4} lg={3} xl={1} key="job type">
                      <div className="form-check mt-5">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="flexCheckDefault"
                          value={filterType}
                          onChange={(e) => setFilterType(!filterType)}
                        />
                        <label className="form-check-label">Fulltime only</label>
                      </div>
                    </Col>
                    <Col xs={12} sm={6} md={4} lg={3} xl={2} className=" mt-4" key="Job search">
                      <Button
                        className="ml-2 mt-1"
                        style={{padding: "9px 19px", borderRadius: "6px"}}
                        onClick={onSearch}
                      >
                        Search
                      </Button>
                    </Col>
                  </Row>
                  <Row className="mb-3">
                    <Col key="tables">
                      {rows
                        ? rows.map((item, index) => {
                            return (
                              <div key={index}>
                                <Row className="flex border-bottom p-2" key={`row-${index}`}>
                                  <Col md={10} key={`row-${item?.title || "-"}`}>
                                    <div className="form-group">
                                      <label
                                        className="font-weight-bold text-primary"
                                        style={{cursor: "pointer"}}
                                        onClick={() => getDetail(item.id)}
                                      >
                                        {item?.title || ""}
                                      </label>
                                      <small className="form-text">
                                        {item?.company} - &nbsp;
                                        <span className="text-uppercase font-weight-bold badge-danger p-1 rounded">
                                          {item?.type}
                                        </span>
                                      </small>
                                    </div>
                                  </Col>
                                  <Col md={2} key={`row-${item?.location}`}>
                                    <div className="form-group">
                                      <label>{item?.location}</label>
                                      <small className="form-text">{item?.created_at}</small>
                                    </div>
                                  </Col>
                                </Row>
                              </div>
                            );
                          })
                        : null}
                    </Col>
                  </Row>
                  {dataPaginate.total ? (
                    <Paginate
                      pageChangeHandler={pageChange}
                      totalRows={dataPaginate.total}
                      rowsPerPage={dataPaginate.limit}
                      selectedPages={dataPaginate.page}
                      pages={Math.ceil(dataPaginate.total / dataPaginate.limit)}
                    />
                  ) : null}
                </Card.Body>
              )}
            </Card>
          </div>
        </div>

        <Modal show={jobId && detail} centered dialogClassName="br-9" size="xl">
          <Modal.Header>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                height: "60px",
              }}
            >
              <h4 style={{fontSize: "24px"}}>Detail</h4>
            </div>
          </Modal.Header>
          <Modal.Body style={{borderRadius: "9px"}}>
            <Row className="mb-2">
              <Col xs={12} sm={12} md={12} lg={12} xl={12} className="mb-3 border-bottom">
                <div className="form-group">
                  <small>
                    {detail.type} / {detail.location}
                  </small>
                  <label className="form-text">{detail.company}</label>
                </div>
              </Col>
            </Row>
            <Row className="flex mb-2">
              <Col xs={12} sm={12} md={12} lg={9} xl={9} className="mb-3 border-bottom pr-5">
                <div className="d-flex align-items-baseline pl-4">
                  <div dangerouslySetInnerHTML={{__html: detail.description}}></div>
                </div>
              </Col>
              <Col xs={12} sm={12} md={12} lg={3} xl={3} className=" mb-3 border-bottom">
                <Row>
                  <Col xs={12} sm={12} md={12} lg={10} xl={12} className="mb-3">
                    <Card>
                      <Card.Header>
                        <label>Company</label>
                      </Card.Header>
                      <Card.Body className="d-flex align-item-center">
                        <Figure>
                          <Figure.Image
                            src={`data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxINEA4NDQ8QDQ0PDQ8PDxAQDRANDhANFREWFhYSExUYHSkgGBolGxUTITEhJSkrLi4uFyAzODMtNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEBAAMBAQEAAAAAAAAAAAAAAQMEBQYCB//EAD0QAAIBAQQECggGAgMBAAAAAAABAgMEESExBRJBURMVMkJhcaGx0eEiM1JjcpKislNzgZHBwiOCQ2LwFP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD9xAAAhQBAUAQFAEBQBAUAQFAEBSAUEAFBCgAQAUEAFAAAEAFBAARQQCgAAAABCgAAAICgCFAAEKAICgCAoAgKAICgCAoAgKAICgAAAAAAAAAAAAAAAAAAAAAAAAAAAAMdStGLSlJJydyv2syAAAAAAAAAAAAAAAhQBAUAQFIBSAACkvF4FIAAAF4AAADBbLVGjG94vYtrYtlqVGN7xexbWzn2Syyry4atlzY714ALJZZV5cNW5PNjlf5HXBQIAUCAAACkAFIUAAQCgAAAAB5+w2ThnU9Nx1Wtl997fT0HoDk6Czrdce+QDib3r+XzI9EXZ1WuteZ0LZX4KEp53LBdLyPN1aspvWm72B1Vof3r+XzHE/vX8vmaNhtbpSWN8G/SWy7eju2y0cFBz25R6ZPIDQ4n96/l8xxP71/L5jQ1rbbpyd7bcovvR1gOTxP71/L5jif3r+XzOsAOTxP71/L5mOtY4ULpzm57oXXaz6ccjpWy1KjG94t8lbWzn2Syyry4atyebHevABY7LKvLhq3J5sdjXVuM1bRjm3KVV/pG5JblidFIoHAt9h4GKlruV8lG667Y3v6DtWbkQ+CPcaWnfVx/MX2yN2zciHwR7gMgKAICgCAoAgKAAIAKAAAAAHI0FnW6498jrnJ0FnW64f2A3NJUXOnJLF4NfozzZ641a9gpzetKOO9NxvA8/QpOpJQjm3+y3m1pW0689VP0IYLpe1m9a9SzU2qcdWUsFtfS8ThgfUJuLUo4NO9Hp7NXVSMZrasVue1HlzoaHtOrLUfJnl0S8wO6a9stUaUb3i3yVtbFstSpRvlnsjtbOfZLLKvLhq3J5sd/kAslllXlw1bk82O/yOugUAAAObp31cfzF9sjds3Ih8Ee40tO+rj+Yvtkbtm5EPgj3AZQAABCgAAAAAEARQAAAAAAcnQWdbrh/Y6xydBZ1v8AT+wHWJeDnaYtOpHg1yp59EQOZb7Tws2+asI9W81yAAbdnpKCVWrlzI5Ob39CJQoqMVVq8nmQ2zfgZM/89fFP1cMtbwiBsWSyyry4atyebHevA66RwbHb2qutN+jL0WtiWy7qO8ANez22NSUoRzjl/wBltaNbS1s1FwcX6Ulj0RONSqODUo4NO9AeqKYLLaFVipL9Vue4zAc7Tvq4/mL7ZG7ZuRD4I9xpad9XH8xfbI3bNyIfBHuQGQAAAAAAAAFAEKCAUEKAAAA5Ogs63XD+x1jk6Czrdce+QHUqTUU5PBJXvqPMWms6knN7Xh0LYjpaatOVJPpl/C/k5AFNmhRUVwtXk8yG2b8BQoqMeFq8nmQ2ze/qMuf+evin6uGWtdlhsiAz/wA9fFP1cMta7uijUr1nUetLPsS3IV6zqScpZ9iW5HwAOxZNJJUnrYzhgltlu/8AdBxwB9VJubcpYtu9nyABtaOtfBSx5Dwkv5PRJ34rI8mdbQ9s/wCKT+Dq3AZdO+rj+Yvtkbtm5EPgj3Glp31cfzF9sjds3Ih8Ee4DIUACAoAhQAICgACACggAoIAKcfQs1F1r2le45tLbI65znoeDx1pdgGKpo+Em5Oum223yfEx1rHTopTlLhPZjgtbyPu02KlQSlJuT2QbXpPwFjsrrS4WtyebHJNbP0A+bLQ4WXC12lHmxb1b11bjLXsMaj1pV1f8A63JbliZq+jI1HrSnJv8AS5LcsD44mh7UuwDBxZT/ABl9PiOLKf4y+nxM/E0Pal2E4mh7UuwDDxZT/GX0+I4sp/jL6fEzcTQ9qXYXiaHtS7AMHFlP8ZfT4jiyn+Mvp8TPxND2pdg4mh7UuwDBxZT/ABl9PiVaNgmmq9zWKfo5/uZuJoe1LsJxND2pdgHzpmadOK1lJqavua9mWNx0LNyIfBHuNHiaHtS7Do046qUVsSX7AfQAAAgAoAAAAAAABCkAAAAYLZao0o3vFvkra2ZzQlo7XqupUlrQwuX8PoA17JZZV5cNW5PNjvXgddBIAAAAAAAAAAAAAAAAAAAAAAAAAAUAQFAEKABCgAAAAMVoqakZTuv1Yt3ZXmU19IeqqfBLuAqtKdN1I43RbueDTSyZkVRXJtpXpZs0rdBwjOcVfGcHGcVvuuU0E4qd9W7V4KGprcnpSv25Ab5jp1L1fJKLvau1k8FtMdgT1NqWtLVTz1NZ3dhq0438Emr1/wDRWweXPA6OusMVjljn1BzSuTaTeWJzalKOpafRV8ZvVwy9FPDdi2W2zT4VXQUlHN41JPVv9FfyB0XJLNpX5Xu4OaWLaS67kc2s/SkmovWpQvcot6ueVyLJJSh6UVT4JKEpRTg3fjtuTyA6QMNjjdBJPWV7ud1yuvyXQZwAAAAACAoAAAAAAAAAAEAFIAKCACkAAC4oAguAAXC4ADDKz4uUZSg5Xa112Nyu2oyU6SilFZLLafRQIAABSACghQBAAKCACggAFIUAQpAAAAAACggAoIAAAAAAAUgAAFAgAApAAABQIAAABQAAAAAACFAAAAAAAAAAAACFAAAAAQCghQAAAAEAoAAAAAAAAAAAAAAAAAAiKAAAAAAAAAAAAAAAAAAAAAAAAAB//9k=`}
                          ></Figure.Image>
                          <Figure.Caption>
                            <a href={detail.company_url}>{detail.company_url}</a>
                          </Figure.Caption>
                        </Figure>
                      </Card.Body>
                    </Card>
                  </Col>
                  <Col xs={12} sm={12} md={12} lg={10} xl={12}>
                    <Card>
                      <Card.Header>
                        <label>How to apply</label>
                      </Card.Header>
                      <Card.Body className="d-flex align-item-stretch">
                        <div dangerouslySetInnerHTML={{__html: detail.how_to_apply}}></div>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col className="text-right">
                <Button
                  onClick={closeDetail}
                  variant="light"
                  style={{padding: "9px 19px", borderRadius: "6px"}}
                >
                  Close
                </Button>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>

        <Modal show={media} centered dialogClassName="br-9" size="xl">
          <Modal.Header>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                height: "60px",
              }}
            >
              <h4 style={{fontSize: "24px"}}>View</h4>
            </div>
          </Modal.Header>
          <Modal.Body style={{borderRadius: "9px"}}>
            <div
              style={{
                width: "auto",
              }}
            >
              <Row className="mb-2">
                <Col xs={12} sm={12} md={12} lg={12} xl={12} className="mb-3 border-bottom">
                  <div
                    dangerouslySetInnerHTML={{
                      __html: `
                      <embed src=${media} width='100%' height='460px' frameBorder='0'></embed>`,
                    }}
                  ></div>
                </Col>
              </Row>
              <Row>
                <Col className="text-right">
                  <Button
                    onClick={closeMedia}
                    variant="light"
                    style={{padding: "9px 19px", borderRadius: "6px"}}
                  >
                    Close
                  </Button>
                </Col>
              </Row>
            </div>
          </Modal.Body>
        </Modal>
      </Fragment>
    </Fragment>
  );
}
