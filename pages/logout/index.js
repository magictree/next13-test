/* eslint-disable react-hooks/exhaustive-deps */
import {deleteCookie} from "cookies-next";
import {useRouter} from "next/router";
import {Fragment, useEffect} from "react";

export default function Logout() {
  const router = useRouter();

  const removeAllAttributes = async () => {
    deleteCookie("token");
    localStorage.removeItem("token");
    localStorage.removeItem("ffullname");
    localStorage.removeItem("email");
    localStorage.removeItem("access");

    router.push("/login");
  };

  useEffect(() => {
    removeAllAttributes();
    return () => {
      true;
    };
  }, []);

  return (
    <Fragment>
      <p className="p-3">Loading...</p>
    </Fragment>
  );
}
