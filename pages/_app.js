import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';
import '../styles/style.css';
import '../styles/Home.module.css';
import 'react-toastify/dist/ReactToastify.css';

import {ToastContainer} from 'react-toastify';
import {SSRProvider} from 'react-bootstrap';

function App({Component, pageProps}) {
   return (
      <SSRProvider>
         <Component {...pageProps} />
         <ToastContainer />
      </SSRProvider>
   );
}
console.log = () => {};
export default App;
