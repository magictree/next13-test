import usersQuery from "../../../models/index";
const scope = 'api::Login::';

const handler = (req, res) => {
  return new Promise(resolve => {
    switch (req.method) {
      case "GET":
        list(req, res, resolve);
        break;
      default: {
        res.status(405).end();
        return resolve();
      }
    }
  });
};

async function list(req, res, resolve) {
  // check di db apakah sudah ada data
  let response = { error:false, message:'success', result:[] }
  try {
    let getResult = await usersQuery.users || null;
    response = { ...response, ...getResult };
    console.log(getResult,"909090")
    res.status(200).json(response);
  } catch (error) {
    console.log({ error, scope:scope+"list" })
    response.error = error;
    res.status(200).json(response);
  }
  return resolve();
}

export default handler;