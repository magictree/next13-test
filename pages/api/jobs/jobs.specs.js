const Joi = require("joi");

const schema = Joi.object().keys({
  filterDescription: Joi.string().default(null),
  filterLocation: Joi.string().default(null),
  filterFullTimeOnly: Joi.boolean().default(false),
  page: Joi.number().positive().allow(null).default(null),
});

const schemaDetail = Joi.object().keys({
  id: Joi.string().required(),
});

/**
 * @function getJobs_Specs validate filter job
 * @param {object} payload
 * @param {string} payload.filterDescription optional type string
 * @param {string} payload.filterLocation optional type string
 * @param {boolean} payload.filterFullTimeOnly optional type boolean
 * @example * let payload = { "filterDescription": "Java", "filterLocation":"Jakarta", "filterFullTimeOnly":false } *
 * @returns {Promise.<{meesage:string, error:boolean, data:payload}>}
 */
exports.getJobs_Specs = async (payload) => {
  let response = {message: "ok", error: false, data: null};
  try {
    const value = await schema.validateAsync(payload);
    response.data = value;
  } catch (error) {
    response.error = true;
    response.message = "Data not found";
  }

  return {...response};
};

/**
 * @function detailJobs_Specs validate detail jobs
 * @param {object} payload
 * @param {string} payload.id is required type string
 * @example * let payload = { "id":"1234" } *
 * @returns {Promise.<{meesage:string, error:boolean, data:payload}>}
 */
exports.detailJobs_Specs = async (payload) => {
  let response = {message: "ok", error: false, data: null};
  try {
    const value = await schemaDetail.validateAsync(payload);
    response.data = value;
  } catch (error) {
    response.error = true;
    response.message = "Data not found";
  }

  return {...response};
};
