"use strict";
import nextConnect from "next-connect";
import middleware from "../../../middleware/auth";
const {detailJobs_Service} = require("./jobs.service");

const handler = nextConnect()
  .use(middleware)
  .get(async (req, res) => {
    const payload = req.query;
    let result = await detailJobs_Service(payload);
    return res.json(result);
  });

export default handler;
