const {
  detailJobs,
  listJobs,
  paginateListJobs,
  paginateValue,
} = require("../../../common/constantsVar");

/**
 * @function getJobs_Module filter job module
 * @param {object} payload
 * @param {string} payload.filterDescription optional type string
 * @param {string} payload.filterLocation optional type string
 * @param {boolean} payload.filterFullTimeOnly optional type boolean
 * @param {number} payload.page optional type number, for paginate
 * @example * let payload = { "filterDescription": "Java", "filterLocation":"Jakarta", "filterFullTimeOnly":false, page:1 } *
 * @returns {Promise.<{meesage:string, error:boolean, data:payload}>}
 */
exports.getJobs_Module = async (payload) => {
  let result = [];
  let {filterDescription, filterLocation, filterFullTimeOnly, page} = payload;
  try {
    let url = listJobs;
    if (page) {
      url = await paginateListJobs(page);
    }
    const hitApi = await fetch(url);
    result = await hitApi.json();

    if (filterFullTimeOnly) {
      result = result.filter((item) => item.type.toLowerCase() === "full time");
    }
    if (filterDescription) {
      result = result.filter(
        (item) => item.title.toLowerCase().search(filterDescription.toLowerCase()) > -1
      );
    }
    if (filterLocation) {
      result = result.filter(
        (item) => item.location.toLowerCase().search(filterLocation.toLowerCase()) > -1
      );
    }
  } catch (error) {
    console.error({error, payload, loc: "::getJobs_Module"});
  }
  paginateValue.page = page;
  return {result, paginate: paginateValue};
};

/**
 * @function detailJobs_Module detail job module
 * @param {object} payload
 * @param {number} payload.id id required get by id
 * @example * let payload = { id:'1234' } *
 * @returns {Promise.<{meesage:string, error:boolean, data:payload}>}
 */
exports.detailJobs_Module = async (payload) => {
  let result = [];
  let {id} = payload;
  try {
    let url = await detailJobs(id);
    const hitApi = await fetch(url);
    result = await hitApi.json();
  } catch (error) {
    console.error({error, payload, loc: "::detailJobs_Module"});
  }
  return result;
};
