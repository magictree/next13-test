const {getJobs_Specs, detailJobs_Specs} = require("./jobs.specs");
const {getJobs_Module, detailJobs_Module} = require("./jobs.module");

/**
 * @function getJobs_Service filter job service
 * @param {object} payload
 * @param {string} payload.filterDescription optional type string
 * @param {string} payload.filterLocation optional type string
 * @param {boolean} payload.filterFullTimeOnly optional type boolean
 * @param {number} payload.page optional type number, for paginate
 * @example * let payload = { "filterDescription": "Java", "filterLocation":"Jakarta", "filterFullTimeOnly":false, page:1 } *
 * @returns {Promise.<{meesage:string, error:boolean, data:payload}>}
 */
exports.getJobs_Service = async (payload) => {
  let result = await getJobs_Specs(payload);
  if (!result.error) {
    result.data = await getJobs_Module(result.data);

    if (result.data.length) {
      result.data = result.data.sort((a, b) => {
        if (a.created_at < b.created_at) {
          return -1;
        } else if (a.created_at == b.created_at) {
          return 0;
        } else {
          return 1;
        }
      });
    }
  }

  return result;
};

/**
 * @function detailJobs_Service detail job service
 * @param {object} payload
 * @param {string} payload.id optional type string
 * @example * let payload = { id: "12345" } *
 * @returns {Promise.<{meesage:string, error:boolean, data:payload}>}
 */
exports.detailJobs_Service = async (payload) => {
  let result = await detailJobs_Specs(payload);
  if (!result.error) {
    result.data = await detailJobs_Module(result.data);
  }

  return result;
};
