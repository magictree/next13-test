"use strict";

const {authLogin_Spec} = require("./auth.spec");
const {authLogin_Module, authVerify_Module} = require("./auth.module");

/**
 * @function authLogin_Service
 * @param {object} payload
 * @param {string} payload.email is required
 * @param {string} payload.password is required
 * @returns { Promise.<{ message:string, error:boolean, data:object }> }
 */
exports.authLogin_Service = async (payload) => {
  let response = {message: "ok", error: false, data: null};
  let result = await authLogin_Spec(payload);

  if (!result.error) {
    result = await authLogin_Module(result.data);

    if (result) {
      response.data = result;
    } else {
      response.message = "user not found";
      response.error = true;
    }
  } else {
    response = result;
  }
  return response;
};

/**
 * @function authVerify_Service
 * @param {object} payload
 * @param {string} payload.token is required
 * @returns { Promise.<{ message:string, error:boolean, data:object }> }
 */
exports.authVerify_Service = async (payload) => {
  let response = {message: "ok", error: false, data: null};
  let result = false;
  result = await authVerify_Module(payload);
  if (result) {
    response.data = result;
  } else {
    response.message = "Unauthorized";
    response.error = true;
  }
  return response;
};
