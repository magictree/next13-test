"use strict";

const Joi = require("joi");
const schemaLogin = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string().invalid("").required(),
});

/**
 * @function authLogin_Spec
 * @param {object} payload
 * @param {string} payload.email is required
 * @param {string} payload.password is required
 * @returns { Promise.<{message: string, error:boolean, data:payload}> }
 */
exports.authLogin_Spec = async (payload) => {
  let response = {message: "ok", error: false, data: null};
  try {
    const result = await schemaLogin.validateAsync(payload);
    response.data = result;
  } catch (error) {
    response.error = true;
    response.message = "user not found";
  }
  return response;
};
