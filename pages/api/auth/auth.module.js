"use strict";

const models = require("../../../models/index");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const KEY = process.env.JWT_KEY;

/**
 * @function authLogin_Module
 * @param {object} payload
 * @param {string} payload.email is required
 * @param {string} payload.password is required
 * @returns { Promise.<token:string> }
 */
exports.authLogin_Module = async (payload) => {
  try {
    let token = false;
    let resultUser = await models.users.findOne({
      where: {email: payload.email},
      attributes: ["id", "email", "fullname", "password"],
      limit: 1,
      raw: true,
    });

    if (resultUser) {
      const matchPassword = await bcrypt.compare(payload.password, resultUser.password);
      if (matchPassword) {
        delete resultUser.password;
        const resultToken = jwt.sign(resultUser, KEY, {expiresIn: 31556926});
        token = {...resultUser, token: resultToken};

        resultUser = await models.users.update(
          {token: resultToken},
          {
            where: {...resultUser.id},
          }
        );
        if (!resultUser) token = false;
      }
    }
    return token;
  } catch (error) {
    console.info(error);
    return false;
  }
};

/**
 * @function authVerify_Module
 * @param {object} payload
 * @param {string} payload.token is required
 * @returns { Promise.<boolean> }
 */
exports.authVerify_Module = async (payload) => {
  let token = false;
  let resultUser = await models.users.findOne({
    where: {token: payload.token},
    attributes: ["id"],
    limit: 1,
  });

  if (resultUser) {
    token = jwt.verify(payload.token, KEY);
  }
  return token;
};
