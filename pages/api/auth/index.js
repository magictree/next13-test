"use strict";
import nextConnect from "next-connect";
import HttpException from "../../../common/errors/HttpExecption";
import {authLogin_Service} from "./auth.service";

/**
 *
 * @param {*} res
 * @param {HttpException} error
 * @returns Response error with custom message
 */
const errorHandler = (res, error) => {
  return res.status(error.statusCode).send({
    success: false,
    message: error.message,
  });
};

const handler = nextConnect().post(async (req, res) => {
  try {
    const result = await authLogin_Service(req.body);
    return res.status(result?.error ? 400 : 200).json(result);
  } catch (error) {
    return errorHandler(res, error);
  }
});

export default handler;
