import {getCookie} from "cookies-next";
import Jobs from "./jobs";

export async function getServerSideProps(context) {
  const {req, res} = context;
  const token = getCookie("token", {req, res});
  if (token) {
    return {
      redirect: {
        permanent: true,
        destination: "/jobs",
      },
      props: {
        token,
      },
    };
  } else {
    return {
      redirect: {
        permanent: true,
        destination: "/login",
      },
      props: {},
    };
  }
}

export default function Home(props) {
  return <Jobs {...props} />;
}
