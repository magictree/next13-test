/**
 *
 * @param {*} res
 * @param {HttpException} error
 * @returns Response error with custom message
 */
const errorHandler = (res, error) => {
  return res.status(error.statusCode).send({
    success: false,
    message: error.message,
  });
};

export default errorHandler;
