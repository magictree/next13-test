'use strict';

/**
 * @class
 * @extends {Error}
 */
class HttpException extends Error {
  /**
   *
   * @param {number} statusCode
   * @param {string} message
   */
  constructor(statusCode, message) {
    super();
    this.message = message;
    this.name = 'HttpException';
    this.statusCode = statusCode;
  }
}

export default HttpException;
