const url = "https://dev6.dansmultipro.com/api/recruitment/positions";

exports.listJobs = `${url}.json`;

/**
 *
 * @param {string} id
 */
exports.detailJobs = async (id = String) => {
  const result = `${url}/${id}`;
  return result;
};

exports.paginateListJobs = async (page = Number) => {
  const result = `${url}.json?page=${page}`;
  return result;
};

exports.paginateValue = {
  limit: 5,
  page: 1,
  total: 999,
};
