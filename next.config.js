/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    BASE_URL: "http://localhost:3000",
    STATIC_URL: "http://localhost:3100",

    MYSQL_HOST: "localhost",
    MYSQL_PORT: "3306",
    MYSQL_USER: "root",
    MYSQL_PASS: "newpassword",
    MYSQL_DBNAME: "db_test",

    JWT_KEY: "e8e25e796f838d7e5aca57315e2bdf24252faff7",
  },
};

module.exports = nextConfig;
